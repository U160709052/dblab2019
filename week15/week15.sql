select * from denormalized;
-- INSERT Movies --
INSERT INTO movies (movie_id, title, ranking, rating, year, votes,duration, oscars, budget)
SELECT distinct movie_id, title, ranking, rating, year, votes,duration, oscars, budget
  FROM denormalized;
  
select * from movies;
-- INSERT Movies --

-- INSERT Countries  --
INSERT INTO countries (country_id, country_name)
SELECT distinct producer_country_id, producer_country_name
  FROM denormalized 
	UNION
		SELECT distinct director_country_id, director_country_name
			FROM denormalized 
				UNION
					SELECT distinct star_country_id, star_country_name
						FROM denormalized;

SELECT * from countries;
-- INSERT Countries  --

-- INSERT Lanugages  --
INSERT INTO languages (movie_id, language_name)  
select distinct movie_id,language_name from denormalized;

select * from languages;
-- INSERT Lanugages  --


-- INSERT Genres  --
INSERT INTO genres (movie_id, genre_name)  
select distinct movie_id,genre_name from denormalized;

select * from genres;
-- INSERT Genres  --

-- INSERT Directors  --
INSERT INTO directors (director_id ,country_id, director_name)  
SELECT distinct director_id, director_country_id,director_name
  FROM denormalized;  
  
select * from directors;
-- INSERT Directors  --

-- INSERT Stars  --
INSERT INTO stars (star_id ,country_id, star_name)  
SELECT distinct star_id, star_country_id,star_name
  FROM denormalized;  
  
select * from stars;
-- INSERT Stars  --

-- INSERT Movie Directors  --
INSERT INTO movie_directors (movie_id ,director_id)  
SELECT distinct movie_id, director_id
  FROM denormalized;  
  
select * from movie_directors;
-- INSERT Movie Directors   --

-- INSERT Movie Stars  --
INSERT INTO movie_stars (movie_id ,star_id)  
SELECT distinct movie_id, star_id
  FROM denormalized;  
  
select * from movie_stars;
-- INSERT Movie Stars  --

-- INSERT Producer Countries  --
INSERT INTO producer_countries (movie_id ,country_id)  
SELECT distinct movie_id, producer_country_id
  FROM denormalized;  
  
select * from producer_countries;
-- INSERT Producer Countries  --







